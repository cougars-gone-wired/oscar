package frc.robot;

import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import frc.robot.Constants.DriveConstants;

public class Encoders {

    private Drive drive;

    public Encoders(Drive drive) {
        this.drive = drive;
    }

    public void resetEncoders() {
        drive.getLeftFrontMotor().setSelectedSensorPosition(0);
        drive.getLeftMiddleMotor().setSelectedSensorPosition(0);
        drive.getLeftRearMotor().setSelectedSensorPosition(0);

        drive.getRightMiddleMotor().setSelectedSensorPosition(0);
        drive.getRightMiddleMotor().setSelectedSensorPosition(0);
        drive.getRightRearMotor().setSelectedSensorPosition(0);

    }

    public double getFrontLeftEncoder() {
        return drive.getLeftFrontMotor().getSelectedSensorPosition();
    }
    public double getMidLeftEncoder() {
        return drive.getLeftMiddleMotor().getSelectedSensorPosition();
    }
    public double getBackLeftEncoder() {
        return drive.getLeftRearMotor().getSelectedSensorPosition();
    }
    public double getFrontRightEncoder() {
        return drive.getRightMiddleMotor().getSelectedSensorPosition();
    }
    public double getMidRightEncoder() {
        return drive.getRightMiddleMotor().getSelectedSensorPosition();
    }
    public double getBackRightEncoder() {
        return drive.getRightRearMotor().getSelectedSensorPosition();
    }

    public double getRawLeftEncoders() {
        return avgSensors(getFrontLeftEncoder(), getMidLeftEncoder(), getBackLeftEncoder());
    }
    public double getRawRightEncoders() {
        return avgSensors(getFrontRightEncoder(), getMidRightEncoder(), getBackRightEncoder());
    }
    public double getAvgRawEncoders() {
        return (Math.abs(getRawLeftEncoders()) + Math.abs(getRawRightEncoders())) / 2;
    }

    public double getLeftEncodersMeters() {
        return ticksToMeters(getRawLeftEncoders()) * (DriveConstants.areLeftEncodersReversed ? -1.0 : 1.0);
    }
    public double getRightEncodersMeters() {
        return ticksToMeters(getRawRightEncoders()) * (DriveConstants.areRightEncodersReversed ? -1.0 : 1.0);
    }
    public double getAvgEncoderMetersAvg() {
        return (getLeftEncodersMeters() + getRightEncodersMeters()) / 2;
    }

    public double getLeftSpeed() {
        return avgSensors(
            drive.getLeftFrontMotor().getSelectedSensorVelocity(), 
            drive.getLeftMiddleMotor().getSelectedSensorVelocity(), 
            drive.getLeftRearMotor().getSelectedSensorVelocity()) 
            * DriveConstants.METER_PER_SECOND_CONSTANT 
            * (DriveConstants.areLeftEncodersReversed ? -1.0 : 1.0);
    }
    public double getRightSpeed() {
        return avgSensors(
            drive.getLeftFrontMotor().getSelectedSensorVelocity(), 
            drive.getLeftMiddleMotor().getSelectedSensorVelocity(), 
            drive.getLeftRearMotor().getSelectedSensorVelocity()) 
            * DriveConstants.METER_PER_SECOND_CONSTANT 
            * (DriveConstants.areRightEncodersReversed ? -1.0 : 1.0);
    }
    public DifferentialDriveWheelSpeeds getWheelSpeeds() {
        return new DifferentialDriveWheelSpeeds(getLeftSpeed(), getRightSpeed());
    }

    public double avgSensors(double sensor1, double sensor2, double sensor3) {
        return (sensor1 + sensor2 + sensor3)/3;
    }

    public double ticksToMeters(double ticks) {
        return ticks * DriveConstants.DISTANCE_PER_TICK;
    }

    public void dashboard() {
        // SmartDashboard.putNumber("Front Left Encoder", getFrontLeftEncoder());
        // SmartDashboard.putNumber("Middle Left Encoder", getMidLeftEncoder());
        // SmartDashboard.putNumber("Back Left Encoder", getBackLeftEncoder());

        // SmartDashboard.putNumber("Front Right Encoder", getFrontRightEncoder());
        // SmartDashboard.putNumber("Middle Right Encoder", getMidRightEncoder());
        // SmartDashboard.putNumber("Back Right Encoder", getBackRightEncoder());

        // SmartDashboard.putNumber("Left Speed", getLeftSpeed());
        // SmartDashboard.putNumber("RightSpeed", getRightSpeed());

        // SmartDashboard.putNumber("Left Encoder", getRawLeftEncoders());
        // SmartDashboard.putNumber("Right Encoder", getRawRightEncoders());
        // SmartDashboard.putNumber("Left Meters", getLeftEncodersMeters());
        // SmartDashboard.putNumber("Right Meters", getRightEncodersMeters());
        // SmartDashboard.putNumber("Encoders", getAvgEncoderMetersAvg());

        // SmartDashboard.putNumber("Front Left Speed", frontLeftSensors.getIntegratedSensorVelocity() * DriveConstants.METER_PER_SECOND_CONSTANT);
        // SmartDashboard.putNumber("Middle Left Speed", middleLeftSensors.getIntegratedSensorVelocity() * DriveConstants.METER_PER_SECOND_CONSTANT);
        // SmartDashboard.putNumber("Back Left Speed", backLeftSensors.getIntegratedSensorVelocity() * DriveConstants.METER_PER_SECOND_CONSTANT);

        // SmartDashboard.putNumber("Front Right Speed", frontRightSensors.getIntegratedSensorVelocity() * DriveConstants.METER_PER_SECOND_CONSTANT);
        // SmartDashboard.putNumber("Middle Right Speed", middleRightSensors.getIntegratedSensorVelocity() * DriveConstants.METER_PER_SECOND_CONSTANT);
        // SmartDashboard.putNumber("Back Right Speed", backRightSensors.getIntegratedSensorVelocity() * DriveConstants.METER_PER_SECOND_CONSTANT);
    }
}