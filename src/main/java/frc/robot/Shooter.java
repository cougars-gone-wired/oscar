package frc.robot;

// Phoenix is used to control and configure the Falcon 500 motor(s)
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
// PercentOutput/Current/Velocity/Position/Follower
// import com.ctre.phoenix.motorcontrol.ControlMode;
// for letting the shooter motor coast instead of brake in neutral mode
import com.ctre.phoenix.motorcontrol.NeutralMode;
// manage sensors
import com.ctre.phoenix.motorcontrol.TalonFXSensorCollection;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
// import edu.wpi.first.wpilibj.Timer;

public class Shooter {
    public static double SHOOTER_SPEED = Constants.ShooterConstants.SHOOTER_SPEED; // -1 to 1  New: 0.75

    private int shooterToggleID;

    /*
    There are two "ShooterModes":
        PID (proportional integral derivative) changes speed over time
        VOLTAGE gives a straight voltage amount to the motors (dead reckoning)
    */
    public enum ShooterStates {
        NOT_MOVING, SHOOTING_VOLTAGE
    }
    private ShooterStates currentShooterState;

    // how fast we have to be going to start feeder when on PID
    public static double PID_INITIAL_VELOCITY_THRESHOLD = 5;
    // how close we have to be to the real value (acceptable error) when on PID
    public static double PID_VELOCITY_THRESHOLD = 175; // 70

    // Voltage constants
    public static double VOLTAGE_TO_VELOCITY = 20480; // position change per 100 ms

    private WPI_TalonFX shooterMotor;
    private TalonFXSensorCollection sensors;

    // private boolean shooterTrigger;
    // private boolean shooterTriggerActive;

    // private double shooterSpeed;
    private double desiredVelocity;

    // used to calculate if the (initial) desired velocity has been reached
    private double initialVelocityThreshold;
    private double velocityThreshold;

    private double velocity;
    private double error;


    public Shooter(int shooterToggleID) {
        this.shooterToggleID = shooterToggleID;

        shooterMotor = new WPI_TalonFX(Constants.ShooterConstants.SHOOTER_MOTOR_ID);
        sensors = new TalonFXSensorCollection(shooterMotor);
        initialize();
    }

    public void initialize() {
        // coast (as opposed to brake) when not giving voltage to the motor
        // removing this might cause the shooter motor to physically break
        shooterMotor.setNeutralMode(NeutralMode.Coast);

        // change motor spin direction (may be removed depending on the robot)
        shooterMotor.setInverted(true);
        SmartDashboard.putNumber("Shooter Power", SHOOTER_SPEED);

        setNotMoving();
    }

    public void controlShooter() {
        boolean shooterToggle = Robot.manipulatorController.getRawButtonPressed(shooterToggleID);
        
        if (shooterToggle) {
            switch (currentShooterState) {
                    case NOT_MOVING:
                        setShooting();
                        break;
                    case SHOOTING_VOLTAGE:
                        setNotMoving();
                        break;
                }
        }


    }

    public void putRealVelocity() {
        SmartDashboard.putNumber("Real Shooter Velocity", velocity);
        SmartDashboard.putNumber("Target Velocity", desiredVelocity);
        SmartDashboard.putBoolean("Desired Velocity", atDesiredVelocity());
        // SmartDashboard.putBoolean("Init Desired Vel", atInitialDesiredVelocity());
        // if (Timer.getFPGATimestamp()%5 == 0) SmartDashboard.putNumber("Real Shooter Velocity", velocity);
    }

    public void setDashboardValues() {
        SHOOTER_SPEED = SmartDashboard.getNumber("Shooter Power", 0.5);

        if (Robot.manipulatorController.getRawButtonPressed(Constants.ControllerConstants.SHOOTER_SPEED_DECREASE_BUTTON)) {
            SHOOTER_SPEED -= Constants.ShooterConstants.SHOOTER_SPEED_ADJUST;
        } else if (Robot.manipulatorController.getRawButtonPressed(Constants.ControllerConstants.SHOOTER_SPEED_INCREASE_BUTTON)) {
            SHOOTER_SPEED += Constants.ShooterConstants.SHOOTER_SPEED_ADJUST;
        }

        SmartDashboard.putNumber("Shooter Power", SHOOTER_SPEED);
    }

    public boolean isNotMoving() {
        return currentShooterState == ShooterStates.NOT_MOVING;
    }

    public boolean isShooting() {
        return (currentShooterState == ShooterStates.SHOOTING_VOLTAGE);
    }

    public void setNotMoving() {
        shooterMotor.set(0);
        // Robot.feeder.setNotMoving();
        currentShooterState = ShooterStates.NOT_MOVING;
    }

    public void setShooting() {
        shooterMotor.set(SHOOTER_SPEED);
        // Robot.feeder.setShooting();
        currentShooterState = ShooterStates.SHOOTING_VOLTAGE;
    }

    public void updateVelocity() {
        velocity = -sensors.getIntegratedSensorVelocity();
        error = desiredVelocity - velocity; // what PID is trying to minimize
    }

    // Feeder uses the following two functions to know when to start or stop feeding the shooter
    public boolean atInitialDesiredVelocity() {
        return Math.abs(error) <= initialVelocityThreshold;
    }

    public boolean atDesiredVelocity() {
        return Math.abs(error) <= velocityThreshold;
    }
}
