package frc.robot;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
// import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

// import frc.robot.Constants.DriveConstants;

public class Drive extends SubsystemBase { // TODO: Understand SubsystemBase (https://first.wpi.edu/FRC/roborio/release/docs/java/edu/wpi/first/wpilibj2/command/SubsystemBase.html)
    
    // The speed adjustment factors on a scale of 0 (never moves) to 1 (full power)
    public static double DRIVE_SPEED = Constants.DriveConstants.DRIVE_SPEED;
    public static double TURN_SPEED = Constants.DriveConstants.TURN_SPEED;

    private int driveSpeedAxisID;
    private int driveTurnAxisID;
    private int driveSideToggleID;

    // Initiate objects
    private WPI_TalonFX leftFrontMotor;
    private WPI_TalonFX leftMiddleMotor;
    private WPI_TalonFX leftRearMotor;
    private SpeedControllerGroup leftMotors;

    private WPI_TalonFX rightFrontMotor;
    private WPI_TalonFX rightMiddleMotor;
    private WPI_TalonFX rightRearMotor;
    private SpeedControllerGroup rightMotors;

    private DifferentialDrive robotDrive;
    // private Encoders encoders;
    // private Gyro gyro;

    public Drive(int driveSpeedAxisID, int driveTurnAxisID, int driveSideToggleID) {
        this.driveSpeedAxisID = driveSpeedAxisID;
        this.driveTurnAxisID = driveTurnAxisID;
        this.driveSideToggleID = driveSideToggleID;

        leftFrontMotor = new WPI_TalonFX(Constants.DriveConstants.LEFT_FRONT_MOTOR_ID);
        leftMiddleMotor = new WPI_TalonFX(Constants.DriveConstants.LEFT_MIDDLE_MOTOR_ID);
        leftRearMotor = new WPI_TalonFX(Constants.DriveConstants.LEFT_REAR_MOTOR_ID);
        leftMotors = new SpeedControllerGroup(leftFrontMotor, leftMiddleMotor, leftRearMotor);

        rightFrontMotor = new WPI_TalonFX(Constants.DriveConstants.RIGHT_FRONT_MOTOR_ID);
        rightMiddleMotor = new WPI_TalonFX(Constants.DriveConstants.RIGHT_MIDDLE_MOTOR_ID);
        rightRearMotor = new WPI_TalonFX(Constants.DriveConstants.RIGHT_REAR_MOTOR_ID);
        rightMotors = new SpeedControllerGroup(rightFrontMotor, rightMiddleMotor, rightRearMotor);

        // encoders = new Encoders(this);
        // gyro = new Gyro();

        initMotors();

        robotDrive = new DifferentialDrive(leftMotors, rightMotors);
        robotDrive.setDeadband(Constants.DriveConstants.DEADZONE);
        robotDrive.setSafetyEnabled(false);

        initalize();
    }

    public void initMotors() {
        // Open Loop Ramp does... // TODO: Research this and complete summary
        leftFrontMotor.configOpenloopRamp(0);
        leftMiddleMotor.configOpenloopRamp(0);
        leftRearMotor.configOpenloopRamp(0);

        rightFrontMotor.configOpenloopRamp(0);
        rightMiddleMotor.configOpenloopRamp(0);
        rightRearMotor.configOpenloopRamp(0);
    }

    public void initalize() {
        // Set all motors to stop when they are initialized
        leftFrontMotor.set(0);
        leftMiddleMotor.set(0);
        leftRearMotor.set(0);

        rightFrontMotor.set(0);
        rightMiddleMotor.set(0);
        rightRearMotor.set(0);

        SmartDashboard.putNumber("Drive Speed", DRIVE_SPEED);
        SmartDashboard.putNumber("Turn Speed", TURN_SPEED);

        // resetOdometry(new Pose2d());

        currentDriveState = DriveStates.SHOOTER_SIDE; // Shooter side is the front by default
    }

    public enum DriveStates { SHOOTER_SIDE, INTAKE_SIDE }

    private DriveStates currentDriveState;

    public void robotDrive() {
        double driveSpeedAxis = Robot.mobilityController.getRawAxis(driveSpeedAxisID) * DRIVE_SPEED;
        double driveTurnAxis = Robot.mobilityController.getRawAxis(driveTurnAxisID) * TURN_SPEED;
        boolean driveSideToggle = Robot.mobilityController.getRawButtonPressed(driveSideToggleID);

        // STATE SWITCHER
        switch (currentDriveState) {
            case SHOOTER_SIDE:
                robotDrive.arcadeDrive(driveSpeedAxis, -driveTurnAxis);

                if (driveSideToggle) setIntakeSide();
                break;

            case INTAKE_SIDE:
                robotDrive.arcadeDrive(-driveSpeedAxis, -driveTurnAxis);
                
                if (driveSideToggle) setShooterSide();
                break;
        }
    }

    public void visionDrive(double driveSpeedAxis, double driveTurnAxis) {
        robotDrive.arcadeDrive(driveSpeedAxis, -driveTurnAxis);
    }

    public void driveStraight(double speed) {
        switch(currentDriveState) {
            case SHOOTER_SIDE:
                robotDrive.curvatureDrive(-speed, 0, false);
                break;
            case INTAKE_SIDE:
                robotDrive.curvatureDrive(speed, 0, false);
                break;
        }
    }

    public void setDashboardValues() {
        DRIVE_SPEED = SmartDashboard.getNumber("Drive Speed", 0.7);
        TURN_SPEED = SmartDashboard.getNumber("Turn Speed", 0.5);
    }

    // Getters (get variable values)
    public boolean isShooterSide() {
        return currentDriveState == DriveStates.SHOOTER_SIDE;
    }
    
    public boolean isIntakeSide() {
        return currentDriveState == DriveStates.INTAKE_SIDE;
    }

    public TalonFX getLeftFrontMotor() {
        return leftFrontMotor;
    }
    public TalonFX getLeftMiddleMotor() {
        return leftMiddleMotor;
    }
    public TalonFX getLeftRearMotor() {
        return leftRearMotor;
    }

    public TalonFX getRightFrontMotor() {
        return rightFrontMotor;
    }
    public TalonFX getRightMiddleMotor() {
        return rightMiddleMotor;
    }
    public TalonFX getRightRearMotor() {
        return rightRearMotor;
    }

    // Setters (sets variable values)
    public void setShooterSide() {
        currentDriveState = DriveStates.SHOOTER_SIDE;
    }

    public void setIntakeSide() {
        currentDriveState = DriveStates.INTAKE_SIDE;
    }

    // Control settings (set various deivce settings)
    public void setMotorsBrake() {
        // Set motors to brake when set to 0 (actively cancel motion; stop the robot)
        leftFrontMotor.setNeutralMode(NeutralMode.Brake);
        leftMiddleMotor.setNeutralMode(NeutralMode.Brake);
        leftRearMotor.setNeutralMode(NeutralMode.Brake);

        rightFrontMotor.setNeutralMode(NeutralMode.Brake);
        rightMiddleMotor.setNeutralMode(NeutralMode.Brake);
        rightRearMotor.setNeutralMode(NeutralMode.Brake);
    }

    public void setMotorsCoast() {
        // Set motors to coast when set to 0 (do not brake)
        leftFrontMotor.setNeutralMode(NeutralMode.Coast);
        leftMiddleMotor.setNeutralMode(NeutralMode.Coast);
        leftRearMotor.setNeutralMode(NeutralMode.Coast);

        rightFrontMotor.setNeutralMode(NeutralMode.Coast);
        rightMiddleMotor.setNeutralMode(NeutralMode.Coast);
        rightRearMotor.setNeutralMode(NeutralMode.Coast);
    }

    // public void resetSensors() {
    //     encoders.resetEncoders();
    //     gyro.zeroHeading();
    // }
}
