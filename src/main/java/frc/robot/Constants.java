package frc.robot;

public class Constants {

    // Controller constants here
    public class ControllerConstants {

        // Mobility Controller
        static final int MOBILITY_CONTROLLER_ID = 0;
        static final int ARM_UP_BUMPER = 6;
        static final int ARM_DOWN_BUMPER = 5;
        static final int CLIMBER_UP_TRIGGER = 3;
        static final int CLIMBER_DOWN_TRIGGER = 2;
        static final int DRIVE_SPEED_AXIS = 1;
        static final int DRIVE_TURN_AXIS = 4;
        static final int DRIVE_SIDE_TOGGLE = 1;
        static final int LIMELIGHT_BUTTON = 2;
        static final int STOP_CAMERA_BUTTON = 8;

        // Manipulator Controller
        static final int MANIPULATOR_CONTROLLER_ID = 1;
        static final int INTAKE_ARM_DOWN_BUMPER = 6;
        static final int INTAKE_ARM_UP_BUMPER = 5;
        static final int INTAKE_AXIS = 1;
        static final int SHOOTER_TRIGGER = 3;
        static final int SHOOTER_MODE_TOGGLE = 8;
        static final int SHOOTER_TOGGLE = 4;
        static final int FEEDER_OUTTAKE_TRIGGER = 2;
        static final int FEEDER_SHOOTER_TRIGGER = 3;
        static final int CHOMPER_OVERRIDE_AXIS = 5;

        static final int SHOOTER_SPEED_INCREASE_BUTTON = 2;
        static final int SHOOTER_SPEED_DECREASE_BUTTON = 3;
        // static final int GREEN_POSITION_BUTTON = 1;
        // static final int YELLOW_POSITION_BUTTON = 2;
        // static final int BLUE_POSITION_BUTTON = 4;
        // static final int RED_POSITION_BUTTON = 3;
    }

    // Drive constants here
    public class DriveConstants {

        // Drive motor controller IDs go here. NOTE: Maybe should be on Settings page on Dashboard?
        public static final int LEFT_FRONT_MOTOR_ID = 6;
        public static final int LEFT_MIDDLE_MOTOR_ID = 7;
        public static final int LEFT_REAR_MOTOR_ID = 8;
        public static final int RIGHT_FRONT_MOTOR_ID = 9;
        public static final int RIGHT_MIDDLE_MOTOR_ID = 10;
        public static final int RIGHT_REAR_MOTOR_ID = 11;

        // Encoder Constants
        public static final int TICKS_PER_REVOLUTION = 2048; // standard for TalonFX
        public static final double WHEEL_DIAMETER = .1524; // in meters
        public static final double GEARING_CONVERSION = 5.3333333;
        public static final double DISTANCE_PER_TICK = (Math.PI * WHEEL_DIAMETER) / (TICKS_PER_REVOLUTION * GEARING_CONVERSION);
        public static final double METER_PER_SECOND_CONSTANT = DISTANCE_PER_TICK / 10; // for converting units per 100ms to meters per second

        public static final boolean areLeftEncodersReversed = false;
        public static final boolean areRightEncodersReversed = true;
        public static final boolean isGyroReversed = false; 
        
        public static final double DEADZONE = 0.15; // Last year's value was 0.15
        public static final double DRIVE_SPEED = 0.7;
        public static final double TURN_SPEED = 0.5;
    }

    // Intake constants here
    public class IntakeConstants {
        public static final double DEADZONE = 0.15;
        public static final int INTAKE_MOTOR_ID = 0;
    }

    // Climber constants here
    public class ClimberConstants {
        public static final double DEADZONE = 0.15;
        public static final int CLIMBER_LEFT_MOTOR_ID = 4;
        public static final int CLIMBER_RIGHT_MOTOR_ID = 5; 
    }

    // Arm movement constants here
    public class ArmConstants {
        public static final int SHOOTER_ARM_TOP_SOLENOID_PORT_1 = 0;
        public static final int SHOOTER_ARM_TOP_SOLENOID_PORT_2 = 1;
        public static final int SHOOTER_ARM_BOTTOM_SOLENOID_PORT_1 = 2;
        public static final int SHOOTER_ARM_BOTTOM_SOLENOID_PORT_2 = 3;
        public static final int INTAKE_ARM_SOLENOID_PORT_1 = 4;
        public static final int INTAKE_ARM_SOLENOID_PORT_2 = 5;
    }

    // Shooter constants here
    public class ShooterConstants{
        public static final double DEADZONE = 0.15;
        public static final int SHOOTER_MOTOR_ID = 1;

        public static final double SHOOTER_SPEED = 0.46;
        public static final double SHOOTER_SPEED_ADJUST = 0.02;
    }

    // Chomper constants here
    public class ChomperConstants {
        public static final int CHOMPER_SOLENOID_PORT = 6;
        public static final double DEADZONE = 0.15;
    }

    public class FeederConstants {
        static final int FEEDER_MOTOR_ID = 2;
        static final int FEEDER_UPPER_LINEBREAK_PORT = 1;
        static final int FEEDER_LOWER_LINEBREAK_PORT = 2;
        public static final double DEADZONE = 0.15;
    }
}
